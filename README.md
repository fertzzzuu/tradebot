# Trading bot that trades stocks better than a professional

## Things it should consider
- Historical price data
    - consider big/small companies and stuff
- Sentiment analysis of RSS feed of finance news.
- Known probability models of the market in economics.


### Links
[first article](https://towardsdatascience.com/the-beginning-of-a-deep-learning-trading-bot-part1-95-accuracy-is-not-enough-c338abc98fc2)